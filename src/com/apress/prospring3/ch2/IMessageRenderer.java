package com.apress.prospring3.ch2;

public interface IMessageRenderer {
	public void render();
	public void setMessageProvider(IMessageProvider provider);
	public IMessageProvider getMessageProvider();
}
