package com.apress.prospring3.ch2;

public interface IMessageProvider {
	public String getMessage();
}
