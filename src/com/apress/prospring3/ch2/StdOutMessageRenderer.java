package com.apress.prospring3.ch2;

public class StdOutMessageRenderer implements IMessageRenderer {
	private IMessageProvider messageProvider = null;	
	@Override
	public void render() {
		if (messageProvider == null) {
			throw new RuntimeException("messageProvider is not set");
		}
		System.out.println(messageProvider.getMessage());
	}

	@Override
	public void setMessageProvider(IMessageProvider provider) {
		this.messageProvider = provider;

	}

	@Override
	public IMessageProvider getMessageProvider() {
		return this.messageProvider;
	}

}
