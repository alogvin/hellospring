package com.apress.prospring3.ch2;

public class HelloSpringMessageProvider implements IMessageProvider {

	@Override
	public String getMessage() {
		return "Hello World!";
	}

}
