package com.apress.prospring3.ch2;

import java.io.FileInputStream;
import java.util.Properties;

public class MessageSupportFactory {
	
	private static MessageSupportFactory instance = null;
	private Properties props = null;
	private IMessageRenderer renderer = null;
	private IMessageProvider provider = null;
	
	private MessageSupportFactory() {
		props = new Properties();
		try {
			props.load(new FileInputStream("src/conf/msf.properties"));
			
			//get the implementation classes
			String rendererClass = props.getProperty("renderer.class");
			String providerClass = props.getProperty("provider.class");
			renderer = (IMessageRenderer) Class.forName(rendererClass).newInstance();
			provider = (IMessageProvider) Class.forName(providerClass).newInstance();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	static {
		instance = new MessageSupportFactory();
	}
	
	public static MessageSupportFactory getInstance() {
		return instance;
	}
	
	public IMessageRenderer getMessageRenderer() {
		return renderer;
	}
	
	public IMessageProvider getMessageProvider() {
		return provider;
	}

}
