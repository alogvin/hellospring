package com.apress.prospring3.ch2;

public class HelloSpring {

	public static void main(String[] args) {
		IMessageRenderer msgRenderer = MessageSupportFactory.getInstance().getMessageRenderer();
		IMessageProvider msgProvider = MessageSupportFactory.getInstance().getMessageProvider();
		msgRenderer.setMessageProvider(msgProvider);
		msgRenderer.render();
	}

}
